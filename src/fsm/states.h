/*
by bcc
2017.05.12
*/

#pragma once
#include "state_context.h"

class ofxUserState : public itg::ofxState<Context> {
protected:
	std::wstring word;
public:
	virtual string getName() { return ""; };
	virtual void stateEnter() {
		ofLogNotice("current state") << getName();
		getSharedData().currentState = getName();
	}
};

class Payment : public ofxUserState {
	ofApp* app;
public:
	string getName() { return "payment"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}	
};

class Idle : public ofxUserState {

public:
	string getName() { return "idle"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
};

class FaceDetected : public ofxUserState {
	ofApp* app;
public:
	string getName() { return "face_detected"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
};



