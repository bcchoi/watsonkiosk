#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxTrueTypeFontUL2.h"
#include "CreditCardScanner.h"
#include "fsm/state_context.h"
#include "ofxPython.h"
#include "ofxJSON.h"
#include "ofxTaskQueue.h"
#define SELFPORT 12345
#define ANDROIDPORT 12349
#define KAIROSFIREBASEPORT 12352
#define DEVICEPORT 12348

struct AndroidInfo {
	AndroidInfo() {
		setuped = false;
		id = 0;
		port = 0;
	}
	bool setuped;
	int id;
	int port;
	ofxOscSender sender;
};

class GiphyTask : public Poco::Task {
	Poco::Random rng;
	const std::string _query;	
	std::vector<std::string>& _filenames;
public:
	GiphyTask(const std::string& name, const std::string query, std::vector<std::string>& filenames) : 
		Poco::Task(name), _query(query), _filenames(filenames) {
		rng.seed();		
	}
	virtual ~GiphyTask() {}
	void runTask() {
		int select = rng.next(100);		
		std::string url = "http://api.giphy.com/v1/gifs/search?q=";
		std::string api_key = "&api_key=dc6zaTOxFJmzC";
		std::string options = "&limit=10&offset="+ofToString(select);
		url += _query;
		url += api_key;
		url += options;
		ofxJSONElement response;
		if (!response.open(url)) {
			ofLogNotice() << "Failed to parse JSON";
		}
		ofLogNotice() << _query;
		int size = response["data"].size();
		for (int i = 0; i < size; i++) {
			string imageurl = response["data"][i]["images"]["original"]["mp4"].asString();			
			if (imageurl.empty()) continue;
			std::vector<std::string> splits;
			splits = ofSplitString(imageurl, "/");
			std::string result = splits[splits.size() - 2];
			std::string filename = _query + "/" + result + ".mp4";
			ofSaveURLTo(imageurl, filename);			
			_filenames.push_back(filename);

			if (sleep(10)) {
				break;
			}
		}
		ofLog() << "Finished";
	}
};

class ofApp : public ofBaseApp {

	// display
	ofxTrueTypeFontUL2 font;
	wstring word;
	int align;

	// communication
	ofxOscReceiver receiver;
	AndroidInfo android;
	ofxOscSender deviceSender;
	ofxOscSender kairosFirebaseSender;

	int pongTime;
	// card scanner
	CreditCardScanner ccScan;
	bool isCardCheck;
	void onSuccessfulSwipe(std::string& data);
	void onStatusChange(CreditCardScanner::Status& data);
	int lastUpdate; // repeat check
	int aliveTime;	// alive check

	// state
	itg::ofxStateMachine<Context> stateMachine;

	ofxPython python;
	ofxPythonObject script;

public:
	void setup();
	void update();
	void draw();
	void exit();
	void keyPressed(int key);


	// Giphy
	ofxJSONElement  response;
	std::vector<std::string> fileNames;
	ofVideoPlayer player;
	ofx::TaskQueue queue;
	int duration;
	Poco::Random rng;
};
