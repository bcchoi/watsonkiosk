#include "ofApp.h"
#include "fsm/states.h"
//--------------------------------------------------------------


char* unicodeToUtf8(const wchar_t* unicode) {
	char* strUtf8 = new char[256];
	memset(strUtf8, 0, 256);
	int nLen = WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), strUtf8, nLen, NULL, NULL);
	return strUtf8;
}
wchar_t* utf8ToUnicode(const char* utf8) {
	wchar_t* strUnicode = new wchar_t[256];
	memset(strUnicode, 0, 256);
	char	strUTF8[256] = { 0, };
	strcpy_s(strUTF8, 256, utf8);
	int nLen = MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), NULL, NULL);
	MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), strUnicode, nLen);
	return strUnicode;
}
char* getAgeWord(int age) {
	if(age < 30)
		return unicodeToUtf8(L"청년");
	if (age < 50)
		return unicodeToUtf8(L"중년");
	if (age < 70)
		return unicodeToUtf8(L"장년");
}
char* getGenderWord(int age, std::string gender) {
	if (gender == "F") {
		if (age < 30)
			return unicodeToUtf8(L"청년 여성");
		if (age < 50)
			return unicodeToUtf8(L"중년 여성");
		if (age < 70)
			return unicodeToUtf8(L"장년 여성");
	}
	else {
		if (age < 30)
			return unicodeToUtf8(L"청년 남성");
		if (age < 50)
			return unicodeToUtf8(L"중년 남성");
		if (age < 70)
			return unicodeToUtf8(L"장년 남성");
	}
}
void ofApp::setup(){

	// ptyhon setup
	python.init();
	python.executeScript("watson.py");
	ofxPythonObject klass = python.getObject("WatsonWorker");
	script = klass();

	// font setup
	font.loadFont("font/210sunflower.ttf", 64, true, true, 0.3f, 0, true);
	font.useProportional(true);
	font.useVrt2Layout(true);
	font.setStrokeWidth(1.0f);
	align = UL2_TEXT_ALIGN_V_MIDDLE | UL2_TEXT_ALIGN_CENTER;
	word = L"준비중..";

	// osc setup
	receiver.setup(SELFPORT);
	deviceSender.setup("localhost", DEVICEPORT);
	kairosFirebaseSender.setup("localhost", KAIROSFIREBASEPORT);

	// printer setup
	// printer
	ccScan.setup();
	ccScan.enableDebugMode();
	isCardCheck = false;
	lastUpdate = 0; // repeat check
	ofAddListener(ccScan.onSuccessfulSwipe, this, &ofApp::onSuccessfulSwipe);
	ofAddListener(ccScan.onStatusChange, this, &ofApp::onStatusChange);

	// state machine setup
	stateMachine.addState<Idle>();
	stateMachine.changeState("idle");

	// giphy
	player.setLoopState(OF_LOOP_NORMAL);
	duration = 100;
	lastUpdate = ofGetElapsedTimeMillis();
	rng.seed();
}

//--------------------------------------------------------------
void ofApp::update(){
	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if (m.getAddress() == "/watsonkiosk") {
			// android
			if (m.getArgAsString(0) == "/android") {
				if (m.getArgAsString(1) == "ping") {					
					std::string ipaddr = m.getArgAsString(2);
					if (android.setuped) {
						m.clear();
						m.setAddress("/android");	// to
						m.addStringArg("/watsonkiosk");	// from
						m.addStringArg("pong");
						android.sender.sendMessage(m);		
						ofLogNotice() << "android alive :" << 
							ofGetElapsedTimeMillis() - pongTime << "msec";
						pongTime = ofGetElapsedTimeMillis();
					}
					else {
						android.id = m.getArgAsInt32(3);
						android.port = m.getArgAsInt32(4);
						android.sender.setup(ipaddr, android.port);
						android.setuped = true;
						ofLogNotice() << "ready for android";
						pongTime = ofGetElapsedTimeMillis();
					}
				}
				else if (m.getArgAsString(1) == "text") {				
					wchar_t* unicode = utf8ToUnicode(m.getArgAsString(2).c_str());					
					word = unicode; delete[] unicode;
					ofxPythonObject func = script.attr("send_message");
					if (func) {										
						int time = ofGetElapsedTimeMillis();
						// utf8 -> utf8
						ofxPythonObject obj = func(ofxPythonObject::fromString(m.getArgAsString(2)));
						if (obj && android.setuped) {
							ofxOscMessage m;
							m.setAddress("/android");		// to
							m.addStringArg("/watsonkiosk");	// from
							m.addStringArg("speechcontinue");							
							m.addStringArg(obj.asString());
							android.sender.sendMessage(m);
						}
						// get attribute data
						// parsing
						// change state
						ofLogNotice() << "watson api: " << ofGetElapsedTimeMillis() - time << "msec";
					}
				}
			}
			// kairosfirebase
			else if (m.getArgAsString(0) == "/kairosfirebase") {
				if (m.getArgAsString(1) == "useridentity") {
					ofxJSONElement json;
					json.parse(m.getArgAsString(2).c_str());
					ofLog() << json["age"].asInt();

					ofxOscMessage m;
					m.setAddress("/android");		// to
					m.addStringArg("/watsonkiosk");	// from
					m.addStringArg("speech");
					char* utf8;
					ofxJSONElement order = json["order"];
					if (order.isNull()) {
						// If you have come before, but do not have a menu registration
						// 또 오셨네요 
						word = L"또 오셨네요";
						utf8 = unicodeToUtf8(word.c_str());
					}
					else {
						ofLog() << order["menu"].asString();
						// 이전에 무슨무슨 메뉴를 주문 하셨군요 
						// 같은 메뉴로 하시겠어요?
						word = L"이전 주문하셨던 메뉴로 하시겠어요";
						utf8 = unicodeToUtf8(word.c_str());
					}	
					
					m.addStringArg(utf8);
					android.sender.sendMessage(m);

					fileNames.clear();
					player.closeMovie();
					queue.cancelAll();
				}
				else if (m.getArgAsString(1) == "firstvisitors") {
					ofxOscMessage m;
					m.setAddress("/android");		// to
					m.addStringArg("/watsonkiosk");	// from
					m.addStringArg("speech");
					word = L"처음 방문 이시군요";
					char* utf8 = unicodeToUtf8(word.c_str());
					m.addStringArg(utf8);
					android.sender.sendMessage(m);

					fileNames.clear();
					player.closeMovie();
					queue.cancelAll();
				}
				else if (m.getArgAsString(1) == "facedetected") {
					ofxJSONElement json;
					json.parse(m.getArgAsString(2).c_str());
					ofLog() << "gender: " << json["gender"].asString();
					ofLog() << "age: " << json["age"].asInt();
					ofLog() << "color: " << json["color"].asString();
					ofLog() << "glasses: " << json["glasses"].asString();

					int age = json["age"].asInt();
					std::string gender = json["gender"].asString();
					char* utf8 = getGenderWord(age, gender);
					ofxOscMessage m;
					m.setAddress("/android");		// to
					m.addStringArg("/watsonkiosk");	// from
					m.addStringArg("speech");
					m.addStringArg(utf8);
					android.sender.sendMessage(m);

					fileNames.clear();
					player.closeMovie();
					queue.cancelAll();
				}
				else if (m.getArgAsString(1) == "ping") {
					ofxOscMessage m;
					m.setAddress("/kairosfirebase");	// to
					m.addStringArg("/watsonkiosk");		// from
					m.addStringArg("pong");					
					kairosFirebaseSender.sendMessage(m);
				}
				else if (m.getArgAsString(1) == "lookatme") {
					if (android.setuped) {
						ofxOscMessage m;
						m.setAddress("/android");		// to
						m.addStringArg("/watsonkiosk");	// from
						m.addStringArg("speech");
						word = L"저를 봐주세요";
						char* utf8 = unicodeToUtf8(word.c_str());
						m.addStringArg(utf8);
						android.sender.sendMessage(m);

						std::vector<std::string> expressions = {
							"neutral","happiness","sadness","surprise","fear","anger",
							"disgust","contempt", "mouth", "tongue", "smile", "eyes", "brow"
						};
						std::vector<std::string> objects = {
							"+dog","+cat",""
						};

						int s1 = rng.next(expressions.size());
						int s2 = rng.next(objects.size());
						queue.cancelAll();
						queue.start(new GiphyTask("task", expressions[s1] + objects[s2], fileNames));
					}
				}
			}
		}
	}

	// scanner
	ccScan.update();
	if (isCardCheck) {
		if (ofGetElapsedTimeMillis() - lastUpdate > 300) {
			ofLogNotice() << "update: " << ccScan.getData();
			isCardCheck = false;
			ofxOscMessage m;
			m.setAddress("/device");	// to
			m.addStringArg("/watsonkiosk");	// from
			m.addStringArg("printer");
			m.addStringArg(ccScan.getData());
			deviceSender.sendMessage(m);
		}
	}

	player.update();
	if (ofGetElapsedTimeMillis() - lastUpdate > duration) {
		lastUpdate = ofGetElapsedTimeMillis();
		if (fileNames.size()) {
			player.closeMovie();
			player.load(fileNames.back());
			fileNames.pop_back();
			player.play();
			int temp = player.getDuration()*1000.0;
			duration = std::max(temp, 3000);
			duration = std::min(duration, 5000);
		}
		else player.closeMovie();
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	int w, h, x, y, mousex, mousey;
	x = 50;
	y = 100;
	mousex = ofGetWidth() * 0.8;
	mousey = ofGetHeight() * 0.9;
	w = mousex - x;
	h = mousey - y;

	//base line
	ofSetColor(255, 127, 255, 255);
	ofDrawLine(0, y, ofGetWidth(), y);
	ofDrawLine(x, 0, x, ofGetHeight());

	//cursor
	ofSetColor(127, 127, 255, 255);
	ofDrawLine(mousex, 0, mousex, ofGetHeight());
	ofDrawLine(0, mousey, ofGetWidth(), mousey);

	ofSetColor(255, 255, 255, 255);
	font.drawString(word, x, y, w, h, align);

	player.draw(0, 0, ofGetWidth(), ofGetHeight());
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

	// speech word
	if (key == '1') {
		if (android.setuped) {
			ofxOscMessage m;
			m.setAddress("/android");		// to
			m.addStringArg("/watsonkiosk");	// from
			m.addStringArg("speech");
			char* utf8 = unicodeToUtf8(L"");
			m.addStringArg(utf8);
			android.sender.sendMessage(m);
			delete[] utf8;
		}
	}
	// stop speech
	else if (key == '2') {
		if (android.setuped) {
			ofxOscMessage m;
			m.setAddress("/android");		// to
			m.addStringArg("/watsonkiosk");	// from
			m.addStringArg("stopspeech");			
			android.sender.sendMessage(m);
		}
	}
	// start
	else if (key == '3') {
		ofxPythonObject func = script.attr("send_message");
		if (func) {
			int time = ofGetElapsedTimeMillis();
			// utf8 -> utf8
			ofxPythonObject obj = func(ofxPythonObject::fromString(""));			
			if (obj && android.setuped) {
				ofxOscMessage m;
				m.setAddress("/android");		// to
				m.addStringArg("/watsonkiosk");	// from
				m.addStringArg("speech");
				m.addStringArg(obj.asString());
				android.sender.sendMessage(m);
			}
			ofLogNotice() << "watson api: " << ofGetElapsedTimeMillis() - time << "msec";
		}
	}
	else if (key == '4') {
		std::vector<std::string> expressions = {
			"neutral","happiness","sadness","surprise","fear","anger",
			"disgust","contempt", "mouth", "tongue", "smile", "eyes", "brow"
		};
		std::vector<std::string> objects = {
			"+dog","+cat",""
		};

		int s1 = rng.next(expressions.size());
		int s2 = rng.next(objects.size());
		queue.cancelAll();
		queue.start(new GiphyTask("task", expressions[s1]+objects[s2], fileNames));
	}
}

//////////////////////////// scanner //////////////////////////////
void ofApp::onSuccessfulSwipe(std::string& number) {
	ofLogVerbose() << "on: " << number;
}
void ofApp::onStatusChange(CreditCardScanner::Status& data) {
	Context& ctx = stateMachine.getSharedData();
	if (data == CreditCardScanner::PROCESSING && ctx.currentState == "payment") {
		isCardCheck = true;
		lastUpdate = ofGetElapsedTimeMillis();
	}
}

void ofApp::exit() {
	queue.cancelAll();
}